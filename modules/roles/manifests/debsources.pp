class roles::debsources {
	ssl::service { 'sources.debian.org':
		notify  => Exec['service apache2 reload'],
		key => true,
	}

	include apache2::ssl
	package { 'libapache2-mod-wsgi': ensure => installed, }
	apache2::site { 'sources.debian.org':
		site => 'sources.debian.org',
		source => 'puppet:///modules/roles/debsources/sources.debian.org.conf',
	}
}
