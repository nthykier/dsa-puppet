class roles::planet_master {
	include apache2::ssl
	apache2::config { 'puppet-debianhosts':
		content => template('roles/conf-debianhostlist.erb'),
	}
	apache2::site { 'planet-master.debian.org':
		source => 'puppet:///modules/roles/planet_master/planet-master.debian.org',
	}
	ssl::service { 'planet-master.debian.org':
		notify => Exec['service apache2 reload'],
		key => true,
	}
}
